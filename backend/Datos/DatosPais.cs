﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;


namespace Datos
{
    public class DatosPais
    {
        
        private static DataTable DT = new DataTable();
       

        public static DataTable AgregarPais(EntidadPais Entidad)
        {
           
                SqlCommand Comando = Conexion.CrearComandoProc("sistema.SPAgregarPais");
                Comando.Parameters.AddWithValue("@_TxtPais", Entidad.TxtPais);
                Comando.Parameters.AddWithValue("@_IdContinente", Entidad.IdContinente);
                Comando.Parameters.AddWithValue("@_TxtCapital", Entidad.TxtCapital);
                Comando.Parameters.AddWithValue("@_intAnioIndependencia", Entidad.IntAnioindependencia);
                Comando.Parameters.AddWithValue("@_IntPoblacion", Entidad.IntPoblacion);
                Comando.Parameters.AddWithValue("@_TxtPresidenteActual", Entidad.TxtPresidenteActual);
                Comando.Parameters.AddWithValue("@_TxtIdiomaOficial", Entidad.TxtIdiomaOficial);
                Comando.Parameters.AddWithValue("@_TxtMoneda", Entidad.TxtMoneda);
    
                DT = Conexion.EjecutarComandoSelect(Comando);
               
            
         

            return DT;
        }


        public static DataTable ObtenerPais(EntidadPais Entidad)
        {

            DT.Clear();

                SqlCommand Comando = Conexion.CrearComandoProc("sistema.SPObtenerPaises");          

                DT = Conexion.EjecutarComandoSelect(Comando);
               

            return DT;

        }


        public static DataTable ObtenerDatosPais(EntidadPais Entidad)
        {

            DT.Clear();

           
                SqlCommand Comando = Conexion.CrearComandoProc("sistema.SPObtenerDatosPaises");
                Comando.Parameters.AddWithValue("@_Idcontinente", Entidad.IdContinente);

                DT = Conexion.EjecutarComandoSelect(Comando);
               

            return DT;

        }


        public static DataTable EliminarPais(Entidades.EntidadPais Entidad)
        {

           
            DT.Clear();

           
                SqlCommand Comando = Conexion.CrearComandoProc("sistema.SPEliminarPais");
                Comando.Parameters.AddWithValue("@_IdPais", Entidad.IdPais);

                DT = Conexion.EjecutarComandoSelect(Comando);
               
            
           

            return DT;

        }

        public static DataTable ActualizarPais(EntidadPais Entidad)
        {

            
            DT.Clear();

         
                SqlCommand Comando = Conexion.CrearComandoProc("sistema.SPActualizarPais");
                Comando.Parameters.AddWithValue("@_IdPais", Entidad.IdPais);
                Comando.Parameters.AddWithValue("@_TxtPais", Entidad.TxtPais);
                Comando.Parameters.AddWithValue("@_IdContinente", Entidad.IdContinente);
                Comando.Parameters.AddWithValue("@_TxtCapital", Entidad.TxtCapital);
                Comando.Parameters.AddWithValue("@_IntAnioIndependencia", Entidad.IntAnioindependencia);
                Comando.Parameters.AddWithValue("@_IntPoblacion", Entidad.IntPoblacion);
                Comando.Parameters.AddWithValue("@_TxtPresidenteActual", Entidad.TxtPresidenteActual);
                Comando.Parameters.AddWithValue("@_TxtIdiomaOficial", Entidad.TxtIdiomaOficial);
                Comando.Parameters.AddWithValue("@_TxtMoneda", Entidad.TxtMoneda);
        

                DT = Conexion.EjecutarComandoSelect(Comando);
                

            return DT;

        }
    }
}
