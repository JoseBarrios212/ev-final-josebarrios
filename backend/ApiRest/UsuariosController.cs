﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Datos;
using Entidades;

namespace ApiRest
{
    public class UsuariosController : ApiController
    {
        [HttpPost]
        [Route("api/AgregarUsuario")]
        public DataTable Agregarusuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.AgregarUsuario(entidad);
        }

        [HttpGet]
        [Route("api/ObtenerUsuarios")]
        public DataTable ObtenerUsuarios()
        {
            return DatosUsuarios.ObtenerUsuarios();
        }


        [HttpPost]
        [Route("api/ObtenerDatosUsuarios")]
        public DataTable ObtenerDatosUsuarios(EntidadUsuarios entidad)
        {
            return DatosUsuarios.ObtenerDatosUsuarios(entidad);
        }


        [HttpPost]
        [Route("api/EliminarUsuario")]
        public DataTable EliminarUsuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.EliminarUsuario(entidad);
        }

        [HttpPost]
        [Route("api/ActualizarUsuario")]
        public DataTable ActualizarUsuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.ActualizarUsuario(entidad);
        }

        [HttpPost]
        [Route("api/InicioDeSesion")]
        public DataTable InicioDeSesion(EntidadUsuarios entidad)
        {
            return DatosUsuarios.InicioDeSesion(entidad);
        }

    }
}