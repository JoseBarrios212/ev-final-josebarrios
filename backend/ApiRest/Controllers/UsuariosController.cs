﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Datos;
using Entidades;

namespace ApiRest
{
    public class UsuariosController : ApiController
    {
        [HttpPost]
        [Route("api/AgregarPais")]
        public DataTable AgregarPais(EntidadPais entidad)
        {
            return DatosPais.AgregarPais(entidad);
        }

        [HttpGet]
        [Route("api/ObtenerPais")]
        public DataTable ObtenerPais(EntidadPais Entidad)
        {
            return DatosPais.ObtenerPais(Entidad);
        }


        [HttpPost]
        [Route("api/ObtenerDatosPais")]
        public DataTable ObtenerDatosPais(EntidadPais entidad)
        {
            return DatosPais.ObtenerDatosPais(entidad);
        }


        [HttpPost]
        [Route("api/EliminarPais")]
        public DataTable EliminarUsuario(EntidadPais entidad)
        {
            return DatosPais.EliminarPais(entidad);
        }

        [HttpPost]
        [Route("api/ActualizarPais")]
        public DataTable ActualizarPais(EntidadPais entidad)
        {
            return DatosPais.ActualizarPais(entidad);
        }

       

       

    }
}